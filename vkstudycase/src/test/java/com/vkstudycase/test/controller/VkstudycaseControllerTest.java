package com.vkstudycase.test.controller;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import com.vkstudycase.vkstudycase.controller.VkStudyCaseController;
import com.vkstudycase.vkstudycase.dto.TitleBasicsDTO;
import com.vkstudycase.vkstudycase.queryservice.NameBasicsQueryService;
import com.vkstudycase.vkstudycase.queryservice.TitleBasicsQueryService;
import com.vkstudycase.vkstudycase.response.ResponseIsActingInaSameProject;
import com.vkstudycase.vkstudycase.response.ResponseTitleBasicsDTOs;
import com.vkstudycase.vkstudycase.util.ApplicationUtils;

public class VkstudycaseControllerTest extends BaseTest {
	public VkstudycaseControllerTest() {
	}

	private TitleBasicsQueryService titleBasicsQueryService;
	private NameBasicsQueryService nameBasicsQueryService;
	private VkStudyCaseController controller;

	@Before
	public void setup() {

		titleBasicsQueryService = ApplicationUtils.createMock(TitleBasicsQueryService.class);
		nameBasicsQueryService = ApplicationUtils.createMock(NameBasicsQueryService.class);
		controller = new VkStudyCaseController(titleBasicsQueryService, nameBasicsQueryService);
	}

	@Test
	public void isActingInaSameProject() throws Exception {
		//case true 
		Mockito.when(nameBasicsQueryService.isActingInaSameProject(Mockito.any())).thenReturn(Boolean.TRUE);
		ResponseIsActingInaSameProject response = controller.isActingInaSameProject("Test1", "TEST2");
		assertThat(response.getResult()).isTrue();
		//case false;		
		Mockito.when(nameBasicsQueryService.isActingInaSameProject(Mockito.any())).thenReturn(Boolean.FALSE);
		response = controller.isActingInaSameProject("Test1", "TEST2");
		assertThat(response.getResult()).isFalse();
		//case null retur false;		
		response = controller.isActingInaSameProject(null, null);
		assertThat(response.getResult()).isFalse();
	}
	


	@Test
	public void getTitleBasicsByPrimaryName() throws Exception {
		
		//return null case
		Mockito.when(nameBasicsQueryService.getKnownForTitlesByPrimaryName(Mockito.anyString())).thenReturn("TEST1");
		Mockito.when(titleBasicsQueryService.getTitleBasicsByKnownForTitles(Mockito.anyString())).thenReturn(null);
		ResponseTitleBasicsDTOs response = controller.getTitleBasicsByPrimaryName("primaryName");
		assertThat(response.getTitleBasicsDTOs()).isEqualTo(null);
		// return data case
		List<TitleBasicsDTO> value=new ArrayList<>();
		value.add(new TitleBasicsDTO());
		Mockito.when(titleBasicsQueryService.getTitleBasicsByKnownForTitles(Mockito.anyString())).thenReturn(value);
		response = controller.getTitleBasicsByPrimaryName("primaryName");
		assertThat(response.getTitleBasicsDTOs()).isNotNull();
	}

}
