package com.vkstudycase.test.controller;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import com.vkstudycase.vkstudycase.model.NameBasics;
import com.vkstudycase.vkstudycase.queryservice.NameBasicsQueryService;
import com.vkstudycase.vkstudycase.repository.NameBasicsRepository;
import com.vkstudycase.vkstudycase.util.ApplicationUtils;

public class NameBasicsQueryTest extends BaseTest {
	public NameBasicsQueryTest() {
	}

	private NameBasicsRepository nameBasicsRepository;
	private NameBasicsQueryService nameBasicsQuery;


	@Before
	public void setup() {

		nameBasicsRepository = ApplicationUtils.createMock(NameBasicsRepository.class);
		nameBasicsQuery = new NameBasicsQueryService(nameBasicsRepository);
	}

	@Test
	public void getKnownForTitlesByPrimaryName() throws Exception {
		NameBasics basics = new NameBasics();
		basics.setBirthYear(1987);
		basics.setKnownForTitles("TEST");
		List<NameBasics> list=new ArrayList<>();
		list.add(basics);
		//not null case
		Mockito.when( nameBasicsRepository.findByPrimaryNameIn(Mockito.any())).thenReturn(list);
		String response = nameBasicsQuery.getKnownForTitlesByPrimaryName("TEST");
		assertThat(response).isNotNull();
		//null case 
		Mockito.when( nameBasicsRepository.findByPrimaryNameIn(Mockito.any())).thenReturn(null);
		 response = nameBasicsQuery.getKnownForTitlesByPrimaryName(null);
		assertThat(response).isNull();
	}

	@Test
	public void isActingInaSameProject() throws Exception {
		NameBasics basics = new NameBasics();
		basics.setBirthYear(1987);
		basics.setKnownForTitles("TEST");
		NameBasics basics2 = new NameBasics();
		basics2.setBirthYear(1987);
		basics2.setKnownForTitles("TEST");
		List<NameBasics> list=new ArrayList<>();
		list.add(basics);
		list.add(basics2);
		//true case
		Mockito.when(nameBasicsRepository.findByPrimaryNameIn(Mockito.any())).thenReturn(list);
		List<String> request=new ArrayList<>();
		request.add("Test1");
		request.add("Test2");
		Boolean response = nameBasicsQuery.isActingInaSameProject(request);
		assertThat(response).isTrue();
		//null case 
		Mockito.when( nameBasicsRepository.findByPrimaryNameIn(Mockito.any())).thenReturn(null);
		 response = nameBasicsQuery.isActingInaSameProject(null);
		assertThat(response).isFalse();
	}




}
