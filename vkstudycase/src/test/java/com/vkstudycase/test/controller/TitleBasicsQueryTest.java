package com.vkstudycase.test.controller;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import com.vkstudycase.vkstudycase.dto.TitleBasicsDTO;
import com.vkstudycase.vkstudycase.model.TitleBasics;
import com.vkstudycase.vkstudycase.queryservice.TitleBasicsQueryService;
import com.vkstudycase.vkstudycase.repository.TitleBasicsRepository;
import com.vkstudycase.vkstudycase.util.ApplicationUtils;

public class TitleBasicsQueryTest extends BaseTest {
	public TitleBasicsQueryTest() {
	}

	private TitleBasicsRepository titleBasicsRepository;
	private TitleBasicsQueryService titleBasicsQueryService;


	@Before
	public void setup() {

		titleBasicsRepository = ApplicationUtils.createMock(TitleBasicsRepository.class);
		titleBasicsQueryService = new TitleBasicsQueryService(titleBasicsRepository);
	}

	@Test
	public void getTitleBasicsByKnownForTitles() throws Exception {

		List<TitleBasics> result=new ArrayList<>();
		TitleBasics titleBasics=new TitleBasics();
		titleBasics.setPrimaryTitle("primaryTitle");
		result.add(titleBasics);
		Mockito.when( titleBasicsRepository.findByTconstIn(Mockito.any())).thenReturn(result);
		List<TitleBasicsDTO> response = titleBasicsQueryService.getTitleBasicsByKnownForTitles("TEST");
		assertThat(response.get(0).getPrimaryTitle()).isNotNull();
	}





}
