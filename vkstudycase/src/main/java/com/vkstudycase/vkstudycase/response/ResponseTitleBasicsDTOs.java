package com.vkstudycase.vkstudycase.response;

import java.util.List;

import com.vkstudycase.vkstudycase.dto.TitleBasicsDTO;

public class ResponseTitleBasicsDTOs extends BaseApiResponse{

	private List<TitleBasicsDTO> titleBasicsDTOs;

	public List<TitleBasicsDTO> getTitleBasicsDTOs() {
		return titleBasicsDTOs;
	}

	public void setTitleBasicsDTOs(List<TitleBasicsDTO> titleBasicsDTOs) {
		this.titleBasicsDTOs = titleBasicsDTOs;
	}








}
