package com.vkstudycase.vkstudycase.dto;

public class TitleBasicsDTO {
	private String tconst;
	private String titleType;
	private String primaryTitle;
	private Integer originalTitle;
	private Boolean isAdult;
	private Integer startYear;
	private Integer endYear;
	private Integer runtimeMinutes;
	private String genres;

	public String getTconst() {
		return tconst;
	}
	public String getTitleType() {
		return titleType;
	}
	public String getPrimaryTitle() {
		return primaryTitle;
	}
	public Integer getOriginalTitle() {
		return originalTitle;
	}
	public Boolean getIsAdult() {
		return isAdult;
	}
	public Integer getStartYear() {
		return startYear;
	}
	public Integer getEndYear() {
		return endYear;
	}
	public Integer getRuntimeMinutes() {
		return runtimeMinutes;
	}
	public String getGenres() {
		return genres;
	}
	public void setTconst(String tconst) {
		this.tconst = tconst;
	}
	public void setTitleType(String titleType) {
		this.titleType = titleType;
	}
	public void setPrimaryTitle(String primaryTitle) {
		this.primaryTitle = primaryTitle;
	}
	public void setOriginalTitle(Integer originalTitle) {
		this.originalTitle = originalTitle;
	}
	public void setIsAdult(Boolean isAdult) {
		this.isAdult = isAdult;
	}
	public void setStartYear(Integer startYear) {
		this.startYear = startYear;
	}
	public void setEndYear(Integer endYear) {
		this.endYear = endYear;
	}
	public void setRuntimeMinutes(Integer runtimeMinutes) {
		this.runtimeMinutes = runtimeMinutes;
	}
	public void setGenres(String genres) {
		this.genres = genres;
	}

}
