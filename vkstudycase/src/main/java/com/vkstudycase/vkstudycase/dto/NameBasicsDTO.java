package com.vkstudycase.vkstudycase.dto;

public class NameBasicsDTO {

	private String nconst;

	private String primaryName;
	private Integer birthYear;
	private Integer deathYear;
	private String knownForTitles;
	private String professions;

	public NameBasicsDTO toDTO() {
		NameBasicsDTO dto = new NameBasicsDTO();
		dto.setPrimaryName(this.getPrimaryName());
		dto.setBirthYear(dto.getBirthYear());
		dto.setDeathYear(dto.getDeathYear());
		dto.setKnownForTitles(this.getKnownForTitles());
		dto.setNconst(this.getNconst());
		dto.setProfessions(this.getProfessions());
		return dto;
	}

	public String getNconst() {
		return nconst;
	}

	public void setNconst(String nconst) {
		this.nconst = nconst;
	}

	public String getPrimaryName() {
		return primaryName;
	}

	public void setPrimaryName(String primaryName) {
		this.primaryName = primaryName;
	}

	public Integer getBirthYear() {
		return birthYear;
	}

	public void setBirthYear(Integer birthYear) {
		this.birthYear = birthYear;
	}

	public Integer getDeathYear() {
		return deathYear;
	}

	public void setDeathYear(Integer deathYear) {
		this.deathYear = deathYear;
	}

	public String getKnownForTitles() {
		return knownForTitles;
	}

	public void setKnownForTitles(String knownForTitles) {
		this.knownForTitles = knownForTitles;
	}

	public String getProfessions() {
		return professions;
	}

	public void setProfessions(String professions) {
		this.professions = professions;
	}

}
