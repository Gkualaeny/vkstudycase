package com.vkstudycase.vkstudycase.dto;

public class TitlePrincipalsDTO {


	private Integer ordering;
	private Integer nconst;
	private Integer category;
	private String job;
	private String characters;
	private String tconst;
	
	public String getTconst() {
		return tconst;
	}
	public Integer getOrdering() {
		return ordering;
	}
	public Integer getNconst() {
		return nconst;
	}
	public Integer getCategory() {
		return category;
	}
	public String getJob() {
		return job;
	}
	public String getCharacters() {
		return characters;
	}
	public void setTconst(String tconst) {
		this.tconst = tconst;
	}
	public void setOrdering(Integer ordering) {
		this.ordering = ordering;
	}
	public void setNconst(Integer nconst) {
		this.nconst = nconst;
	}
	public void setCategory(Integer category) {
		this.category = category;
	}
	public void setJob(String job) {
		this.job = job;
	}
	public void setCharacters(String characters) {
		this.characters = characters;
	}



}
