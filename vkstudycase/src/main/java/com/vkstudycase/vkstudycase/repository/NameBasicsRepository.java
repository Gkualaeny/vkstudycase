package com.vkstudycase.vkstudycase.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vkstudycase.vkstudycase.model.NameBasics;

public interface NameBasicsRepository  extends JpaRepository<NameBasics, Long> {


	List<NameBasics> findByPrimaryNameIn (List<String> primaryNames);

}
