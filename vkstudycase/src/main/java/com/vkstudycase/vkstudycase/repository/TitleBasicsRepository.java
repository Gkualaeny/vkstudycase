package com.vkstudycase.vkstudycase.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vkstudycase.vkstudycase.model.TitleBasics;

public interface TitleBasicsRepository  extends JpaRepository<TitleBasics, Long> {


	List<TitleBasics> findByTconstIn (List<String> tconsts);

}
