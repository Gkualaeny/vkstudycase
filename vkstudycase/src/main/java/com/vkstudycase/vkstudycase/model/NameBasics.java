package com.vkstudycase.vkstudycase.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "nameBasics")
public class NameBasics {

	@NotNull
	@Id
	@Column(name="nconst")
	private String nconst;
	@NotNull
	@Column(name="primaryName")
	private String primaryName;
	@NotNull
	@Size(min = 1880, max = 2100)
	@Column(name="birthYear")
	private Integer birthYear;
	@Size(min = 1880, max = 2100)
	@Column(name="deathYear")
	private Integer deathYear;
	@Column(name="knownForTitles")
	private String knownForTitles;
	@Column(name="professions")
	private String professions;
	public String getNconst() {
		return nconst;
	}
	public void setNconst(String nconst) {
		this.nconst = nconst;
	}
	public String getPrimaryName() {
		return primaryName;
	}
	public void setPrimaryName(String primaryName) {
		this.primaryName = primaryName;
	}
	public Integer getBirthYear() {
		return birthYear;
	}
	public void setBirthYear(Integer birthYear) {
		this.birthYear = birthYear;
	}
	public Integer getDeathYear() {
		return deathYear;
	}
	public void setDeathYear(Integer deathYear) {
		this.deathYear = deathYear;
	}
	public String getKnownForTitles() {
		return knownForTitles;
	}
	public void setKnownForTitles(String knownForTitles) {
		this.knownForTitles = knownForTitles;
	}
	public String getProfessions() {
		return professions;
	}
	public void setProfessions(String professions) {
		this.professions = professions;
	}


}
