package com.vkstudycase.vkstudycase.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import com.vkstudycase.vkstudycase.dto.TitleBasicsDTO;
import com.vkstudycase.vkstudycase.dto.TitlePrincipalsDTO;

@Entity
@Table(name = "titlePrincipals")
public class TitlePrincipals {

	@Id
	@GeneratedValue
	private Integer ordering;
	@Size(min = 1880, max = 2100)
	private Integer nconst;
	@Size(min = 1880, max = 2100)
	private Integer category;
	private String job;
	private String characters;
	private String tconst;

	public TitlePrincipalsDTO toDTO() {
		TitlePrincipalsDTO dto = new TitlePrincipalsDTO();
		dto.setCategory(this.getCategory());
		dto.setCharacters(this.getCharacters());
		dto.setJob(this.getJob());
		dto.setNconst(this.getNconst());
		dto.setOrdering(this.getOrdering());
		dto.setTconst(this.getTconst());
		return dto;
	}

	public String getTconst() {
		return tconst;
	}

	public Integer getOrdering() {
		return ordering;
	}

	public Integer getNconst() {
		return nconst;
	}

	public Integer getCategory() {
		return category;
	}

	public String getJob() {
		return job;
	}

	public String getCharacters() {
		return characters;
	}

	public void setTconst(String tconst) {
		this.tconst = tconst;
	}

	public void setOrdering(Integer ordering) {
		this.ordering = ordering;
	}

	public void setNconst(Integer nconst) {
		this.nconst = nconst;
	}

	public void setCategory(Integer category) {
		this.category = category;
	}

	public void setJob(String job) {
		this.job = job;
	}

	public void setCharacters(String characters) {
		this.characters = characters;
	}

}
