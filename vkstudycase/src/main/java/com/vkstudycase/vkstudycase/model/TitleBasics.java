package com.vkstudycase.vkstudycase.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.vkstudycase.vkstudycase.dto.TitleBasicsDTO;

@Entity
@Table(name = "titleBasics")
public class TitleBasics {
	@Id
	@Column(name = "tconst")
	private String tconst;
	@Column(name = "titleType")
	private String titleType;
	@Column(name = "primaryTitle")
	private String primaryTitle;
	@Column(name = "originalTitle")
	private Integer originalTitle;
	@Column(name = "isAdult")
	private Boolean isAdult;
	@Column(name = "startYear")
	private Integer startYear;
	@Column(name = "endYear")
	private Integer endYear;
	@Column(name = "runtimeMinutes")
	private Integer runtimeMinutes;
	@Column(name = "genres")
	private String genres;

	public TitleBasicsDTO toDTO() {
		TitleBasicsDTO dto = new TitleBasicsDTO();
		dto.setEndYear(this.getEndYear());
		dto.setGenres(this.getGenres());
		dto.setIsAdult(this.getIsAdult());
		dto.setOriginalTitle(this.getOriginalTitle());
		dto.setPrimaryTitle(this.getPrimaryTitle());
		dto.setRuntimeMinutes(this.getRuntimeMinutes());
		dto.setStartYear(this.getStartYear());
		dto.setTconst(this.getTconst());
		dto.setTitleType(this.getTitleType());
		return dto;
	}

	public String getTconst() {
		return tconst;
	}

	public String getTitleType() {
		return titleType;
	}

	public String getPrimaryTitle() {
		return primaryTitle;
	}

	public Integer getOriginalTitle() {
		return originalTitle;
	}

	public Boolean getIsAdult() {
		return isAdult;
	}

	public Integer getStartYear() {
		return startYear;
	}

	public Integer getEndYear() {
		return endYear;
	}

	public Integer getRuntimeMinutes() {
		return runtimeMinutes;
	}

	public String getGenres() {
		return genres;
	}

	public void setTconst(String tconst) {
		this.tconst = tconst;
	}

	public void setTitleType(String titleType) {
		this.titleType = titleType;
	}

	public void setPrimaryTitle(String string) {
		this.primaryTitle = string;
	}

	public void setOriginalTitle(Integer originalTitle) {
		this.originalTitle = originalTitle;
	}

	public void setIsAdult(Boolean isAdult) {
		this.isAdult = isAdult;
	}

	public void setStartYear(Integer startYear) {
		this.startYear = startYear;
	}

	public void setEndYear(Integer endYear) {
		this.endYear = endYear;
	}

	public void setRuntimeMinutes(Integer runtimeMinutes) {
		this.runtimeMinutes = runtimeMinutes;
	}

	public void setGenres(String genres) {
		this.genres = genres;
	}

}
