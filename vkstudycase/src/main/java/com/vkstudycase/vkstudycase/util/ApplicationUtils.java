package com.vkstudycase.vkstudycase.util;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.time.LocalDate;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.text.WordUtils;
import org.mockito.Mockito;

import com.vkstudycase.vkstudycase.constant.Constants;
import com.vkstudycase.vkstudycase.enums.ExceptionEnum;

public class ApplicationUtils {

	private ApplicationUtils() {
		// Constructors
	}

	public static Boolean isListEmptyOrNull(List<?> list) {
		return list == null || list.isEmpty();
	}

	public static String concater(String... texts) {
		StringBuilder builder = new StringBuilder();
		for (String item : texts) {
			builder = builder.append(item);
		}
		return builder.toString();
	}


	// ORNEK APPROVER_DATE i approverDate string olarak donusturur
	public static String getCamelCase(String text) {
		String result =StringUtils.remove(WordUtils.capitalizeFully(text, '_'), "_");
		   char c[] = result.toCharArray();
		    c[0] = Character.toLowerCase(c[0]);
		    return new String(c);
	}

	public static void throwBusinessException(Boolean condition, ExceptionEnum exceptionEnum) throws Exception {
		if (condition) {
			throw new Exception(exceptionEnum.getValue());
		}
	}

	
	public static <T> T genericValueSetter(String methodName, Object value, T modal) {
		StringBuilder validMethodName = new StringBuilder();
		validMethodName = !methodName.contains(Constants.SET)
				? validMethodName.append(Constants.SET).append(getCamelCase(methodName))
				: validMethodName.append(getCamelCase(methodName));
		for (Method item : modal.getClass().getDeclaredMethods()) {
			if (item.getName().contains(validMethodName.toString())) {
				try {
					Method method=modal.getClass().getDeclaredMethod(item.getName(), LocalDate.class);
					method.invoke(null, value);
					break;
				} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		}
		return modal;
	}
	public static <T> T createMock(Class<T> classToMock) {
		return Mockito.mock(classToMock);
	}
}
