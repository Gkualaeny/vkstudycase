package com.vkstudycase.vkstudycase.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.vkstudycase.vkstudycase.dto.TitleBasicsDTO;
import com.vkstudycase.vkstudycase.queryservice.NameBasicsQueryService;
import com.vkstudycase.vkstudycase.queryservice.TitleBasicsQueryService;
import com.vkstudycase.vkstudycase.response.ResponseIsActingInaSameProject;
import com.vkstudycase.vkstudycase.response.ResponseTitleBasicsDTOs;
import com.vkstudycase.vkstudycase.util.ApplicationUtils;

import io.swagger.annotations.Api;

@RestController
@RequestMapping("/vkStudyCaseController")
@Api(value = "vkStudyCaseController")
public class VkStudyCaseController {
	private TitleBasicsQueryService titleBasicsQueryService;
	private NameBasicsQueryService nameBasicsQueryService;

	@Autowired
	public VkStudyCaseController(TitleBasicsQueryService titleBasicsQueryService,
			NameBasicsQueryService nameBasicsQueryService) {
		this.titleBasicsQueryService = titleBasicsQueryService;
		this.nameBasicsQueryService = nameBasicsQueryService;

	}

	/**/
	@ResponseStatus(HttpStatus.OK)
	@GetMapping("/get/title/basics/by/primary/name/{primaryName}/")
	public ResponseTitleBasicsDTOs getTitleBasicsByPrimaryName(@PathVariable @NotNull String primaryName) {
		ResponseTitleBasicsDTOs response = new ResponseTitleBasicsDTOs();
		String knownForTitles = nameBasicsQueryService.getKnownForTitlesByPrimaryName(primaryName);
		List<TitleBasicsDTO> result = titleBasicsQueryService.getTitleBasicsByKnownForTitles(knownForTitles);
		if (!ApplicationUtils.isListEmptyOrNull(result)) {
			response.setTitleBasicsDTOs(result);
		}
		response.responseSuccessSetter();
		return response;
	}

	@ResponseStatus(HttpStatus.OK)
	@GetMapping("/is/acting/in/a/same/project/{primaryName1}/{primaryName2}")
	public ResponseIsActingInaSameProject isActingInaSameProject(@PathVariable @NotNull String primaryName,
			@PathVariable @NotNull String primaryName2) {
		ResponseIsActingInaSameProject response = new ResponseIsActingInaSameProject();
		List<String> primaryNames = new ArrayList<>();
		primaryNames.add(primaryName);
		primaryNames.add(primaryName2);
		response.setResult(nameBasicsQueryService.isActingInaSameProject(primaryNames));
		response.responseSuccessSetter();
		return response;
	}

}
