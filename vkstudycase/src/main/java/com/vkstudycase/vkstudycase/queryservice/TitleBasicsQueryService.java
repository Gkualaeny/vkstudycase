package com.vkstudycase.vkstudycase.queryservice;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vkstudycase.vkstudycase.constant.Constants;
import com.vkstudycase.vkstudycase.dto.TitleBasicsDTO;
import com.vkstudycase.vkstudycase.model.TitleBasics;
import com.vkstudycase.vkstudycase.repository.TitleBasicsRepository;
import com.vkstudycase.vkstudycase.util.ApplicationUtils;

@Service
public class TitleBasicsQueryService implements ITitleBasicsQueryService {
	private TitleBasicsRepository titleBasicsRepository;

	@Autowired
	public TitleBasicsQueryService(TitleBasicsRepository titleBasicsRepository) {
		this.titleBasicsRepository = titleBasicsRepository;
	}

	@Override
	public List<TitleBasicsDTO> getTitleBasicsByKnownForTitles(String knownForTitles) {
		List<String> request = new ArrayList<>();
		List<TitleBasicsDTO> result = new ArrayList<>();
		if (knownForTitles != null) {
			for (String item : knownForTitles.split(Constants.COMA)) {
				request.add(item);
			}
			List<TitleBasics> modelList = titleBasicsRepository.findByTconstIn(request);
			if (!ApplicationUtils.isListEmptyOrNull(modelList)) {
				for (TitleBasics item : modelList) {
					result.add(item.toDTO());
				}
			}
		}

		return result;
	}

}
