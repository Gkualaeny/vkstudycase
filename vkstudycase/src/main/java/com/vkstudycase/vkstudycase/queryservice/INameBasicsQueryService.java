package com.vkstudycase.vkstudycase.queryservice;

import java.util.List;

public interface INameBasicsQueryService {

	Boolean isActingInaSameProject(List<String> primaryNames);

	String getKnownForTitlesByPrimaryName(String primaryName);

}
