package com.vkstudycase.vkstudycase.queryservice;

import java.util.List;

import com.vkstudycase.vkstudycase.dto.TitleBasicsDTO;

public interface ITitleBasicsQueryService {

	List<TitleBasicsDTO> getTitleBasicsByKnownForTitles(String knownForTitles);



}
