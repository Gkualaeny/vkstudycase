package com.vkstudycase.vkstudycase.queryservice;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vkstudycase.vkstudycase.constant.Constants;
import com.vkstudycase.vkstudycase.model.NameBasics;
import com.vkstudycase.vkstudycase.repository.NameBasicsRepository;
import com.vkstudycase.vkstudycase.util.ApplicationUtils;

@Service
public class NameBasicsQueryService implements INameBasicsQueryService {
	private NameBasicsRepository nameBasicsRepository;

	@Autowired
	public NameBasicsQueryService(NameBasicsRepository nameBasicsRepository) {
		this.nameBasicsRepository = nameBasicsRepository;
	}

	@Override
	public Boolean isActingInaSameProject(List<String> primaryNames) {
		List<NameBasics> resultList = nameBasicsRepository.findByPrimaryNameIn(primaryNames);
		Boolean result = Boolean.FALSE;
		if (!ApplicationUtils.isListEmptyOrNull(resultList) && resultList.size() > 1) {
			String[] firstKnownForTitles = resultList.get(0) != null && resultList.get(0).getKnownForTitles() != null
					? resultList.get(0).getKnownForTitles().split(Constants.COMA)
					: new String[0];
			String secondKnownForTitles = resultList.get(1) != null ? resultList.get(1).getKnownForTitles()
					: Constants.EMPTY;
			for (String item : firstKnownForTitles) {
				if (item != null && secondKnownForTitles.contains(item)) {
					result = Boolean.TRUE;
					break;
				}
			}

		}
		return result;
	}

	@Override
	public String getKnownForTitlesByPrimaryName(String primaryName) {
		List<String> primaryNames = new ArrayList<>();
		primaryNames.add(primaryName);
		List<NameBasics> resultList = nameBasicsRepository.findByPrimaryNameIn(primaryNames);
		return !ApplicationUtils.isListEmptyOrNull(resultList) && resultList.get(0) != null
				? resultList.get(0).getKnownForTitles()
				: null;
	}
}
