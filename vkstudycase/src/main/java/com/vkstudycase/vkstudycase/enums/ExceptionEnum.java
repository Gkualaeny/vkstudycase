package com.vkstudycase.vkstudycase.enums;

public enum ExceptionEnum {
	
    ORDER_DATE_NOT_PAST("Sipriş tarihi bugünün tarihinden ileri bir tarih olmalıdır.Lütfen ileri bir tarih seçiniz."),
    ORDER_COUNT_NOT_LOWER_THAN_ONE("Sipariş adedi 1'den küçük bir değer olamaz."),
	ORDER_ID_NOT_VALID("Geçerli bir id iletmediğiniz için işleminize devam edilemiyor.Lütfen geçerli bir id iletiniz.");


    private final String value;
 

    ExceptionEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }	
}