package com.vkstudycase.vkstudycase;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
@SpringBootApplication
public class VkstudycaseApplication {

	public static void main(String[] args) {
		SpringApplication.run(VkstudycaseApplication.class, args);
	}

}
